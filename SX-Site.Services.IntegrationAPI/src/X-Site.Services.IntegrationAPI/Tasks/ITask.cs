﻿namespace X_Site.Services.IntegrationAPI.Tasks
{
    public interface ITask
    {
        void Execute();
    }
}
