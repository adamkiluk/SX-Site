﻿namespace X_Site.Services.IntegrationAPI
{
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using Consul;
    using Hangfire;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Reflection;
    using X_Site.Common;
    using X_Site.Common.Mongo;
    using X_Site.Common.MVC;
    using X_Site.Common.RabbitMq;
    using X_Site.Services.IntegrationAPI.Handlers.Champions;
    using X_Site.Services.IntegrationAPI.Messages.Events;

    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IContainer Container { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCustomMvc();
            //services.AddSwaggerDocs();
            //services.AddConsul();
            //services.AddJaeger();
            //services.AddOpenTracing();
            services.AddInitializers(typeof(IMongoDbInitializer));

            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly())
                .AsImplementedInterfaces();
            builder.Populate(services);
            //builder.AddDispatchers();
            builder.AddRabbitMq();
            builder.AddMongo();

            Container = builder.Build();

            return new AutofacServiceProvider(Container);
        }

        public void Configure(IApplicationBuilder app, 
            IHostingEnvironment env,
            IApplicationLifetime applicationLifetime, 
            IConsulClient client,
            IStartupInitializer startupInitializer)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "local")
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseRabbitMq();
                //.SubscribeCommand<CreateOrder>(onError: (c, e) =>
                //    new CreateOrderRejected(c.Id, c.CustomerId, e.Message, e.Code))
                //.SubscribeCommand<ApproveOrder>(onError: (c, e) =>
                //    new ApproveOrderRejected(c.Id, e.Message, e.Code))
                //.SubscribeCommand<CancelOrder>(onError: (c, e) =>
                //    new CancelOrderRejected(c.Id, c.CustomerId, e.Message, e.Code))
                //.SubscribeCommand<RevokeOrder>(onError: (c, e) =>
                //    new RevokeOrderRejected(c.Id, c.CustomerId, e.Message, e.Code))
                //.SubscribeCommand<CompleteOrder>(onError: (c, e) =>
                //    new CompleteOrderRejected(c.Id, c.CustomerId, e.Message, e.Code))
                //.SubscribeCommand<CreateOrderDiscount>()
                //.SubscribeEvent<CustomerCreated>(@namespace: "customers");

            RecurringJob.AddOrUpdate<ImportBriefChampionsHandler>(x => x.Execute(), "*/1 * * * *");

            //applicationLifetime.ApplicationStopped.Register(() =>
            //{
            //    client.Agent.ServiceDeregister(consulServiceId);
            //    Container.Dispose();
            //});

            startupInitializer.InitializeAsync();

        }
    }
}
