﻿namespace X_Site.Services.IntegrationAPI
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Config
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
