﻿namespace X_Site.Services.IntegrationAPI.Repositories
{
    using System;
    using System.Threading.Tasks;
    using X_Site.Common.Mongo;
    using X_Site.Services.IntegrationAPI.Models.BriefChampion;

    public class ChampionRepository : IChampionRepository
    {
        private readonly IMongoRepository<BriefChampion> _briefChampionsRepository;

        public ChampionRepository(IMongoRepository<BriefChampion> briefChampionsRepository)
            => _briefChampionsRepository = briefChampionsRepository;

        public async Task AddAsync(BriefChampion briefChampion)
            => await _briefChampionsRepository.AddAsync(briefChampion);

        public async Task<BriefChampion> GetBriefChampionAsync(Guid id)
            => await _briefChampionsRepository.GetAsync(id);

        public async Task<bool> CheckVersion(BriefChampion briefChampion)
        {
            var version = _briefChampionsRepository.ExistAsync(x => x.Version == briefChampion.Version);
            return version.Result;
        }
    }
}
