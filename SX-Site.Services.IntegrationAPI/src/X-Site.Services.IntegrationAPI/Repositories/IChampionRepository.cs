﻿namespace X_Site.Services.IntegrationAPI.Repositories
{
    using System;
    using System.Threading.Tasks;
    using X_Site.Services.IntegrationAPI.Models.BriefChampion;

    public interface IChampionRepository
    {
        Task<BriefChampion> GetBriefChampionAsync(Guid id);
        Task AddAsync(BriefChampion champion);
        Task<bool> CheckVersion(BriefChampion champion);
    }
}
