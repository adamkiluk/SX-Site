﻿namespace X_Site.Services.IntegrationAPI.Messages.Events
{
    using Newtonsoft.Json;
    using System;
    using X_Site.Common.Messages;

    public class ImportBriefChampionsRejected : IRejectedEvent
    {
        public Guid Id { get; }
        public string Reason { get; }
        public string Code { get; }

        [JsonConstructor]
        public ImportBriefChampionsRejected(Guid id, string reason, string code)
        {
            Id = id;
            Reason = reason;
            Code = code;
        }
    }
}
