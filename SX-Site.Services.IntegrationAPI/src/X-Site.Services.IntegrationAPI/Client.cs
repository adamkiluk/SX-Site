﻿namespace X_Site.Services.IntegrationAPI
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using X_Site.Common.HttpClient;

    public interface IClient
    {
        Task<HttpResponse<T>> GetBriefChampionList<T>();
    }

    public class Client : IClient
    {
        private readonly Common.HttpClient.Client _httpClient;
        private readonly Config _config;

        public Client(Config config)
        {
            _config = config;
            _httpClient = new Common.HttpClient.Client();
        }

        public Task<HttpResponse<T>> GetBriefChampionList<T>()
        {
            throw new NotImplementedException();
        }
    }
}
