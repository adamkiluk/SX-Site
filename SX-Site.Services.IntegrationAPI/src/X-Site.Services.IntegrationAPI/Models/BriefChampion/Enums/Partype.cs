﻿namespace X_Site.Services.IntegrationAPI.Models.BriefChampion.Enums
{
    public enum Partype
    {
        BloodWell,
        Courage,
        CrimsonRush,
        Energy,
        Ferocity,
        Flow,
        Fury,
        Heat,
        Mana,
        None,
        Rage,
        Shield
    }
}
