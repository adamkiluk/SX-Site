﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace X_Site.Services.IntegrationAPI.Models.BriefChampion.Enums
{
    public enum Sprite
    {
        Champion0Png,
        Champion1Png,
        Champion2Png,
        Champion3Png,
        Champion4Png
    }
}
