﻿namespace X_Site.Services.IntegrationAPI.Models.BriefChampion.Enums
{
    public enum Tag
    {
        Assassin,
        Fighter,
        Mage,
        Marksman,
        Support,
        Tank
    }
}
