﻿namespace X_Site.Services.IntegrationAPI.Models.BriefChampion
{
    using Newtonsoft.Json;
    using X_Site.Services.IntegrationAPI.Models.BriefChampion.Enums;

    public class Image
    {
        [JsonProperty("full")]
        public string Full { get; set; }

        //[JsonProperty("sprite")]
        //public Sprite Sprite { get; set; }

        [JsonProperty("x")]
        public long X { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }

        [JsonProperty("w")]
        public long W { get; set; }

        [JsonProperty("h")]
        public long H { get; set; }
    }
}
