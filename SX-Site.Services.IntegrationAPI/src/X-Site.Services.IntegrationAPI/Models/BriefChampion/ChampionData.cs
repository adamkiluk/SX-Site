﻿namespace X_Site.Services.IntegrationAPI.Models.BriefChampion
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using X_Site.Services.IntegrationAPI.Models.BriefChampion.Enums;

    public class ChampionData
    {
        [JsonProperty("version")]
        public Version Version { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("blurb")]
        public string Blurb { get; set; }

        [JsonProperty("info")]
        public Info Info { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("tags")]
        public List<Tag> Tags { get; set; }

        //[JsonProperty("partype")]
        //public Partype Partype { get; set; }

        [JsonProperty("stats")]
        public Dictionary<string, double> Stats { get; set; }
    }
}
