﻿namespace X_Site.Services.IntegrationAPI.Models.BriefChampion
{
    using Newtonsoft.Json;

    public class Info
    {
        [JsonProperty("attack")]
        public long Attack { get; set; }

        [JsonProperty("defense")]
        public long Defense { get; set; }

        [JsonProperty("magic")]
        public long Magic { get; set; }

        [JsonProperty("difficulty")]
        public long Difficulty { get; set; }
    }
}
