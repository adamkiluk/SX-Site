﻿namespace X_Site.Services.IntegrationAPI.Models.BriefChampion
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using X_Site.Common.Types;

    public class BriefChampion : IIdentifiable
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("data")]
        public Dictionary<string, ChampionData> Data { get; set; }
    }
}
