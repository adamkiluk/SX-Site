﻿namespace X_Site.Services.IntegrationAPI.Handlers.Champions
{
    using Newtonsoft.Json.Linq;
    using System;
    using System.IO;
    using System.Net;
    using System.Threading;
    using X_Site.Common.RabbitMq;
    using X_Site.Services.IntegrationAPI.Models.BriefChampion;
    using X_Site.Services.IntegrationAPI.Repositories;
    using X_Site.Services.IntegrationAPI.Tasks;

    public class ImportBriefChampionsHandler : ITask
    {
        private static readonly object Lock = new object();
        private readonly IClient _client;
        private readonly IChampionRepository _championRepository;
        private readonly IBusPublisher _busPublisher;

        public ImportBriefChampionsHandler(IBusPublisher busPublisher,
            IChampionRepository championRepository,
            IClient client)
        {
            _busPublisher = busPublisher;
            _championRepository = championRepository;
            _client = client;
        }


        public void Execute()
        {
            // Do not wait for lock acquire
            if (!Monitor.TryEnter(Lock)) return;

            try
            {
                ExecuteInternal();
            }
            catch (Exception)
            {

            }
            finally
            {
                Monitor.Exit(Lock);
            }
        }

        private void ExecuteInternal()
        {
            WebRequest request = WebRequest.Create("http://ddragon.leagueoflegends.com/cdn/9.20.1/data/en_US/champion.json");

            WebResponse response = request.GetResponse();

            Stream stream = response.GetResponseStream();

            StreamReader reader = new StreamReader(stream);

            string responseFromServer = reader.ReadToEnd();

            JObject parsedString = JObject.Parse(responseFromServer);

            BriefChampion champions = parsedString.ToObject<BriefChampion>();

            _championRepository.

            _championRepository.AddAsync(champions);
        }
    }
}
