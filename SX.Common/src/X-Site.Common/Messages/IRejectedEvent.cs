﻿namespace X_Site.Common.Messages
{

    public interface IRejectedEvent : IEvent
    {
        string Reason { get; }
        string Code { get; }
    }
}
