﻿namespace X_Site.Common.Mongo
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using X_Site.Common.Types;

    public interface IMongoRepository<TEntity> where TEntity : IIdentifiable
    {
        Task<TEntity> GetAsync(Guid id);
        Task AddAsync(TEntity entity);
        Task<bool> ExistAsync(Expression<Func<TEntity, bool>> predicate);
    }
}
