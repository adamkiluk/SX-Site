﻿namespace X_Site.Common.Mongo
{
    using System.Threading.Tasks;

    public interface IMongoDbSeeder
    {
        Task SeedAsync();
    }
}
