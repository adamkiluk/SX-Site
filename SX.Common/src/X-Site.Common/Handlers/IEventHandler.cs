﻿namespace X_Site.Common.Handler
{
    using System.Threading.Tasks;
    using X_Site.Common.Messages;
    using X_Site.Common.RabbitMq;

    public interface IEventHandler<in TEvent> where TEvent : IEvent
    {
        Task HandleAsync(TEvent @event, ICorrelationContext context);
    }
}
