﻿namespace X_Site.Common.Handler
{
    using System.Threading.Tasks;
    using X_Site.Common.RabbitMq;
    using X_Site.Common.Messages;

    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        Task HandleAsync(TCommand command, ICorrelationContext context);
    }
}
