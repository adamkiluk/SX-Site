﻿namespace X_Site.Common
{
    using System;
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using X_Site.Common.Mongo;
    using X_Site.Common.MVC;

    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IContainer Container { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddInitializers(typeof(IMongoDbInitializer));

            var builder = new ContainerBuilder();

            //rewiriting current  microsoft services and add them to autofac
            builder.RegisterAssemblyTypes(typeof(Startup).Assembly)
                .AsImplementedInterfaces();
            builder.Populate(services);

            builder.AddMongo();

            Container = builder.Build();

            return new AutofacServiceProvider(Container);
            
        }
    }
}
