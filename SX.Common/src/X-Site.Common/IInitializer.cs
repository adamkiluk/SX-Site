﻿namespace X_Site.Common
{
    using System.Threading.Tasks;

    public interface IInitializer
    {
        Task InitializeAsync();
    }
}
