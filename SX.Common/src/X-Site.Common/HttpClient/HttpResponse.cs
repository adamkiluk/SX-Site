﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace X_Site.Common.HttpClient
{
    public class HttpResponse<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Body { get; set; }
        public T Data { get; set; }

        public Exception Exception { get; set; }
    }
}
