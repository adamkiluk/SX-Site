﻿namespace X_Site.Common.HttpClient.Dump
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Text;

    public class RequestDump
    {
        public string Method { get; set; }

        public string Address { get; set; }

        public string ContentType { get; set; }

        public long ContentLength { get; set; }

        public IDictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();

        public string Body { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static RequestDump FromHttpWebRequest(HttpWebRequest request)
        {
            var retval = new RequestDump()
            {
                Method = request.Method,
                Address = request.Address.ToString(),
                ContentType = request.ContentType,
                ContentLength = request.ContentLength
            };

            foreach (var key in request.Headers.AllKeys)
            {
                retval.Headers.Add(key, request.Headers[key]);
            }

            return retval;
        }
    }
}
