﻿namespace X_Site.Common.HttpClient.Dump
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Text;

    public class ResponseDump
    {
        public string CharacterSet { get; set; }

        public long ContentLength { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        public IDictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();

        public string Body { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static ResponseDump FromHttpWebResponse(HttpWebResponse response)
        {
            var retval = new ResponseDump()
            {
                CharacterSet = response.CharacterSet,
                ContentLength = response.ContentLength,
                StatusCode = response.StatusCode
            };

            foreach (var key in response.Headers.AllKeys)
            {
                retval.Headers.Add(key, response.Headers[key]);
            }

            return retval;
        }
    }
}
