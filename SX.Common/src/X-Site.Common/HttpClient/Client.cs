﻿namespace X_Site.Common.HttpClient
{
    using Newtonsoft.Json;
    using System;
    using System.IO;
    using System.Net;
    using System.Threading.Tasks;
    using X_Site.Common.HttpClient.Dump;

    public class Client
    {
        private RequestDump _requestDump;
        private ResponseDump _responseDump;

        public RequestDump GetRequestDump()
        {
            return _requestDump;
        }

        public ResponseDump GetResponseDump()
        {
            return _responseDump;
        }

        public async Task<HttpResponse<T>> GetAsync<T>(string url, RequestOptions options)
        {
            return await Task.Run(() =>
            {
                ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

                if (options.Query.Count > 0)
                {
                    if (url.Contains("&", StringComparison.InvariantCultureIgnoreCase))
                    {
                        url += "&";
                    }
                    else if (!url.Contains("?", StringComparison.InvariantCultureIgnoreCase))
                    {
                        url += "?";
                    }

                    url += options.Query;
                }

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
                httpWebRequest.Method = "GET";

                httpWebRequest.Accept = string.IsNullOrWhiteSpace(options.Accept)
                    ? "application/json"
                    : options.Accept;

                httpWebRequest.ContentType = string.IsNullOrWhiteSpace(options.ContentType)
                    ? "application/json"
                    : options.ContentType;

                foreach (var (key, value) in options.Headers)
                {
                    if (key.Equals("Authorization", StringComparison.InvariantCultureIgnoreCase))
                    {
                        httpWebRequest.PreAuthenticate = true;
                    }

                    httpWebRequest.Headers.Add(key, value);
                }

                _requestDump = RequestDump.FromHttpWebRequest(httpWebRequest);

                var httpWebResponse = GetResponseNoException(httpWebRequest);

                _responseDump = ResponseDump.FromHttpWebResponse(httpWebResponse);

                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream() ?? throw new InvalidOperationException(), options.Encoding))
                {
                    var result = streamReader.ReadToEnd();

                    _responseDump.Body = result;

                    var response = new HttpResponse<T>()
                    {
                        StatusCode = httpWebResponse.StatusCode,
                        Body = result,
                        Data = default(T)
                    };

                    try
                    {
                        response.Data = JsonConvert.DeserializeObject<T>(result);
                    }
                    catch (Exception ex)
                    {
                        response.Exception = ex;
                    }

                    return response;
                }
            });
        }
        private static HttpWebResponse GetResponseNoException(HttpWebRequest req)
        {
            try
            {
                return (HttpWebResponse)req.GetResponse();
            }
            catch (WebException we)
            {
                if (!(we.Response is HttpWebResponse resp)) throw;

                return resp;
            }
        }
    }
}
