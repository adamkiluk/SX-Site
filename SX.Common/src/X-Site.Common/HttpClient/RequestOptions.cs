﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace X_Site.Common.HttpClient
{
    public class RequestOptions
    {
        public IDictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();
        public string Accept { get; set; }

        public string ContentType { get; set; } = "application/json";

        public Encoding Encoding { get; set; } = Encoding.UTF8;

        public NameValueCollection Query { get; set; } = new NameValueCollection();
    }
}
