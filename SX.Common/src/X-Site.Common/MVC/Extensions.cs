﻿namespace X_Site.Common.MVC
{
    using System;
    using System.Linq;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class Extensions
    {
        public static IMvcCoreBuilder AddCustomMvc(this IServiceCollection services)
        {
            using(var serviceProvider = services.BuildServiceProvider())
            {
                var configuration = serviceProvider.GetService<IConfiguration>();
                services.Configure<AppOptions>(configuration.GetSection("app"));
            }

            //should add custom services configuration

            //TODO

            return services
                .AddMvcCore()
                .AddJsonFormatters()
                .AddDataAnnotations()
                .AddApiExplorer()
                //custom json options method
                .AddAuthorization();
        }

        public static IServiceCollection AddInitializers(this IServiceCollection services, params Type[] initializers)
                    => initializers == null
                    ? services
                    : services.AddTransient<IStartupInitializer, StartupInitializer>(c =>
                    {
                        var startupInitializer = new StartupInitializer();
                        var validInitializers = initializers.Where(t => typeof(IInitializer).IsAssignableFrom(t));
                        foreach (var initializer in validInitializers)
                        {
                            startupInitializer.AddInitializer(c.GetService(initializer) as IInitializer);
                        }

                        return startupInitializer;
                    });
    }
}
