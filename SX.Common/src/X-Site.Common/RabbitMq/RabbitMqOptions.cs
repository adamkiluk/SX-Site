﻿namespace X_Site.Common.RabbitMq
{
    using RawRabbit.Configuration;

    public class RabbitMqOptions : RawRabbitConfiguration
    {
        public string Namespace { get; set; }
        public int Retries { get; set; }
        public int RetryInterval { get; set; }
    }
}
