﻿namespace X_Site.Common.RabbitMq
{
    using System.Threading.Tasks;
    using X_Site.Common.Handler;
    using X_Site.Common.Messages;

    public interface IBusPublisher
    {
        Task SendAsync<TCommand>(TCommand command, ICorrelationContext context)
            where TCommand : ICommand;

        Task PublishAsync<TEvent>(TEvent @event, ICorrelationContext context)
            where TEvent : IEvent;
    }
}
