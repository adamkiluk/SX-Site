﻿namespace X_Site.Common.RabbitMq
{
    using System;
    using X_Site.Common.Messages;
    using X_Site.Common.Types;

    //Marker
    public interface IBusSubscriber
    {
        IBusSubscriber SubscribeCommand<TCommand>(string @namespace = null, string queueName = null,
            Func<TCommand, X_SiteException, IRejectedEvent> onError = null)
            where TCommand : ICommand;

        IBusSubscriber SubscribeEvent<TEvent>(string @namespace = null, string queueName = null,
            Func<TEvent, X_SiteException, IRejectedEvent> onError = null)
            where TEvent : IEvent;
    }
}
