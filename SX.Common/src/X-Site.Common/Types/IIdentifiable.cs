﻿namespace X_Site.Common.Types
{
    using System;

    public interface IIdentifiable
    {
        Guid Id { get; }
    }
}
