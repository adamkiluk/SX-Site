﻿namespace X_Site.Common.Types
{
    using System;

    public class X_SiteException : Exception
    {
        public string Code { get; }

        public X_SiteException()
        {
            //empty constructor to non relational DB as good practice
        }

        public X_SiteException(string code)
        {
            Code = code;
        }
    }
}
