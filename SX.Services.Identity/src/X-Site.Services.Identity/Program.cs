﻿namespace X_Site.Services.Identity
{
    using System;
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                   .UseStartup<Startup>();
                   //.UseLogging()
                   //.UseVault()
                   //.UseLockBox()
                   //.UseAppMetrics();
    }
}
